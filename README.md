# YAN64E
Yet Another Nintendo 64 Emulator. <br  />
How surprising, also it is made in Rust.

# How to build YAN64E
Install Rust 1.10 stable and type cargo build.

# How to use YAN64E
Type cargo run then pass the N64 Rom.