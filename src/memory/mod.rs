// Our internal crates
pub mod interconnect;
pub mod ram;
// Our use cases
use byteorder::{ByteOrder, BigEndian};