// N64 8MB Ram, we just expect to include the expansion pack
const RAM_SIZE: usize = 0x800000;

pub struct Ram {
    data: Box<[u16]>,
}

impl Ram {
    pub fn new() -> Ram {
        Ram {
            data: vec![0; RAM_SIZE].into_boxed_slice(),
        }
    }
}