// Our use cases
use super::ram;

pub struct Interconnect {
    ram: ram::Ram,
}

impl Interconnect {
    pub fn new() -> Interconnect {
        Interconnect {
            ram: ram::Ram::new(),
        }
    }
}