// Our use cases
use super::memory;

// Number of registers
const NUM_GPR: usize = 32;
const NUM_FPR: usize = 32;

pub struct Cpu {
    pub interconnect: memory::interconnect::Interconnect,
    reg_gpr: [u64; NUM_GPR],
    reg_fpr: [u64; NUM_FPR],
    reg_pc: u64,
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            interconnect: memory::interconnect::Interconnect::new(),
            reg_gpr: [0; NUM_GPR],
            reg_fpr: [0; NUM_FPR],
            reg_pc: 0,
        }
    }

    pub fn run_instruction(&mut self) {
        self.reg_pc += 4;
    }
}