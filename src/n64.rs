// Our use cases
use std::path;
use super::cpu;
use super::memory;

pub struct Nintendo64 {
    pub core: cpu::Cpu,
}

impl Nintendo64 {
    pub fn new() -> Nintendo64 {
        Nintendo64 {
            core: cpu::Cpu::new(),
        }
    }

    pub fn load_rom(&mut self, rom_path: &String) {
        
    }

    pub fn run(&mut self) {
        loop {
            self.core.run_instruction();
        }
    }
}