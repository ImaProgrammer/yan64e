// Our external crates
extern crate byteorder;
extern crate rustyline;
// Our internal crates
mod n64;
mod cpu;
mod memory;
mod debugger;
// Our use cases
use std::env;

fn main() {
    // Gather the rom and emulator
    let args: Vec<String> = env::args().collect();
    let rom = &args[0];
    let mut nintendo64 = n64::Nintendo64::new();

    // Load the rom and run
    nintendo64.load_rom(rom);
    nintendo64.run();
}
